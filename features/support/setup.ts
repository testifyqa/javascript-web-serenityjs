import { setDefaultTimeout } from 'cucumber';
import { Actors } from '../../spec/screenplay/Actors';
import { engage } from '@serenity-js/core';

setDefaultTimeout(10000); // set default cucumber timeout
engage(new Actors()); // instantiate our Actors object so they can use their given abilities