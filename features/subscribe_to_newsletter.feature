Feature: Subscribe to newsletter
 
  As Ned (a reader of email newsletters)
  I want to stay up-to-date with recommended restaurants
  So that I always have a choice of new restaurants to try out

  Scenario: Subscribing to the newsletter with a valid email address
    Given that Ned decides to use Zagat to stay updated with recommended restaurants
    When he tries to subscribe to the newsletter with his email address of 'ned@flanders.com'
    Then he is asked to also provide his zip code to accompany 'ned@flanders.com'

  Scenario: Subscribing to the newsletter with an invalid email address
    Given that Ned decides to use Zagat to stay updated with recommended restaurants
    When he tries to subscribe to the newsletter with his email address of 'nedstark'
    Then he is prevented by validation that contains a message about using an invalid "email address."