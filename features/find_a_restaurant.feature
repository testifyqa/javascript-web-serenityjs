Feature: Find a restaurant
 
  As Frank (a lover of food)
  I want to find recommended restaurants to eat in
  So that I can feed my appetite with great cuisine
 
  Scenario: Searching for all restaurants in San Diego
    Given that Frank decides to use Zagat to find recommended restaurants
    When he searches for all restaurants in his area of 'San Diego'
    Then a list of recommended restaurants in 'San Diego' are returned to him

  Scenario: Searching for all restaurants in a location that does not exist
    Given that Frank decides to use Zagat to find recommended restaurants
    When he searches for all restaurants in his area of 'ABCDEFG Town'
    Then no places are found