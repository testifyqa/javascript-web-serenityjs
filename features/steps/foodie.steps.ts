import { Given, When, Then } from "cucumber";
import { actorCalled, actorInTheSpotlight } from "@serenity-js/core";
import { Navigate, Website, isVisible } from '@serenity-js/protractor';
import { SearchForRestaurants } from "../../spec/screenplay/tasks/SearchForRestaurants";
import { Ensure, endsWith, equals } from "@serenity-js/assertions";
import { ZagatPlacesWidget } from "../../spec/screenplay/ui/ZagatPlacesWidget";
import { NoReturnedResults } from "../../spec/screenplay/NoReturnedResults";

Given(/that (.*) decides to use Zagat to (?:find|stay updated with) recommended restaurants/, (actorName: string) => 
    actorCalled(actorName).attemptsTo(Navigate.to('/national'))
);

When('he/she searches for all restaurants in his/her area of {string}', (location: string) =>
    actorInTheSpotlight().attemptsTo(SearchForRestaurants.in(location))
);
  
Then('a list of recommended restaurants in {string} are returned to him/her', (expectedLocation: string) => {
    const uriEncodedLocation = encodeURIComponent(expectedLocation.trim()); // replace spaces in 'expectedLocation' string with '%20'
    return actorInTheSpotlight().attemptsTo(
        Ensure.that(Website.url(), endsWith(uriEncodedLocation)), // check url we land on ends with 'San%20Diego'
        Ensure.that(ZagatPlacesWidget.returnedResults, isVisible()) // check that results are returned
    )
});

Then('no places are found', () =>
    actorInTheSpotlight().attemptsTo(Ensure.that(NoReturnedResults(), equals('No places found')))
);