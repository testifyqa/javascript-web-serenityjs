import { When, Then } from "cucumber";
import { actorInTheSpotlight, See } from "@serenity-js/core";
import { SubscribeToNewsletter } from "../../spec/screenplay/tasks/SubscribeToNewsletter";
import { Ensure, includes, equals, contain } from "@serenity-js/assertions";
import { Website, isVisible, Attribute } from "@serenity-js/protractor";
import { ZagatNewsletterSubscriptionWidget } from "../../spec/screenplay/ui/ZagatNewsletterSubscriptionWidget";
import { EmailValidationMessage } from "../../spec/screenplay/EmailValidationMessage";

When('he/she tries to subscribe to the newsletter with his/her email address of {string}', (emailAddress: string) => 
    actorInTheSpotlight().attemptsTo(SubscribeToNewsletter.with(emailAddress))
);

Then('he/she is asked to also provide his/her zip code to accompany {string}', (expectedEmailAddress: string) => 
    actorInTheSpotlight().attemptsTo(
        Ensure.that(Website.url(), includes('/newsletters?email='+expectedEmailAddress)), // check url we land on contains expected email address
        Ensure.that(ZagatNewsletterSubscriptionWidget.zipCodeField, isVisible()) // check that zip code field is displayed
    )
);

Then('he/she is prevented by validation that contains a message about using an invalid {string}', (expectedValidationMessage: string) =>
    actorInTheSpotlight().attemptsTo(Ensure.that(EmailValidationMessage(), includes(expectedValidationMessage))) // check that returned validation message is same as expected one
);