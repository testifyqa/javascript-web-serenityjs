module.exports = {
    root: true,
    parser: '@typescript-eslint/parser', // tell ESLint to use the parser package we installed so it understands TypeScript syntax
    plugins: [
      '@typescript-eslint', // load the plugin package we installed so we can use the rules within our codebase
    ],
    extends: [ // ESLint config extends the following given configurations...
      'eslint:recommended',
      'plugin:@typescript-eslint/eslint-recommended',
      'plugin:@typescript-eslint/recommended',
    ],
    rules: {
      "@typescript-eslint/explicit-function-return-type": "off",
    }
  };