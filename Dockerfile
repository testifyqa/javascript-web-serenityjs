#Initialise a new build stage and sets the Base Image for subsequent instructions.
#As such, a valid Dockerfile must start with a FROM instruction. The image can be any valid image.
#Here we are getting an image running Linux Alpine OS which has NodeJS 13.7 on it
FROM node:13.7-alpine3.10

#Add a bash shell to the base image defined above
RUN apk add --no-cache bash

#create a directory called 'app'
RUN mkdir /app

#The WORKDIR instruction sets the working directory for any RUN, CMD, ENTRYPOINT, COPY and ADD instructions that follow it in the Dockerfile.
#Here we are setting the app directory we created above as the working directory
WORKDIR /app

#Copy all files and folders from our project root in to our 'app' working directory
COPY . /app

#Install all the npm packages
RUN npm install

#An ENTRYPOINT allows you to configure a container that will run as an executable.
ENTRYPOINT ["/bin/sh", "-c", "npm test"]