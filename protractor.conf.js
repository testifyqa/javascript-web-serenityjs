const
    { Photographer, TakePhotosOfFailures } = require('@serenity-js/protractor'),
    { SerenityBDDReporter } = require('@serenity-js/serenity-bdd'),
    { ConsoleReporter } = require('@serenity-js/console-reporter'),
    { ArtifactArchiver } = require('@serenity-js/core');

exports.config = { // export the config so it is accessible by other files
    seleniumAddress: 'http://hub:4444/wd/hub', // use our selenium server
    baseUrl: 'https://www.zagat.com', // // set the base url of our system under test
    directConnect: false, // your test script communicates with the Selenium Server now
    allScriptsTimeout: 120000, // set global protractor timeout to 120 seconds (2 minutes)
    multiCapabilities: [ {
        browserName: 'chrome', // set this test browser to 'chrome'
        shardTestFiles: true, // specs are sharded by file when set to true
        maxInstances: 2 // maximum number of browser instances that can run in parallel for this
    }, {
        browserName: 'firefox', // set this test browser to 'firefox'
        shardTestFiles: true, // specs are sharded by file when set to true
        maxInstances: 2 // maximum number of browser instances that can run in parallel for this
    }],
    specs: ['./features/*.feature'], // set the location of our tests to any and all feature files in the 'features' folder
    framework: 'custom',
    frameworkPath: require.resolve('@serenity-js/protractor/adapter'), // this serenity-js module needs to be called as a custom framework
    onPrepare: () => {
        require('ts-node').register({ // register 'ts-node' to resolve all our package imports
            project: './tsconfig.json' // and handle TypeScript execution smoothly
        });
    },
    serenity: {
        //actors: new Actors(), // instantiate our Actors object so they can use their abilities
        runner: 'cucumber', // tell serenity-js that we want to use the Cucumber runner
        crew: [
            Photographer.whoWill(TakePhotosOfFailures), // or Photographer.whoWill(TakePhotosOfInteractions),
            new SerenityBDDReporter(),
            ConsoleReporter.forDarkTerminals(), // display execution summary in console
            ArtifactArchiver.storingArtifactsAt('./target/site/serenity') // store serenity reports at given location
        ]
    },
    cucumberOpts: {
        require: ['./features/**/*.ts', //require our step definition files
                  './support/**/*.ts'], // also require any necessary files in support folders and sub-folders (e.g. setup.ts)        
                  format: ['snippets:pending-steps.txt'], // workaround to get pending step definitions
        compiler: 'ts:ts-node/register' //interpret step definitions as TypeScript
    },
};
