import { Task } from '@serenity-js/core';
import { Enter, Click } from '@serenity-js/protractor';
import { Key } from 'protractor';
import { ZagatFindAPlaceWidget } from "../ui/ZagatFindAPlaceWidget";

export const SearchForRestaurants = {
    in: (location: string) =>
        Task.where(`#actor searches for recommended restaurants in ${ location }`,
            Click.on(ZagatFindAPlaceWidget.initialSearchField),
            Click.on(ZagatFindAPlaceWidget.locationSearchField),
            Enter.theValue(location + Key.RETURN).into(ZagatFindAPlaceWidget.locationSearchField),
        ),
};