import { Task } from '@serenity-js/core';
import { Enter, Click } from '@serenity-js/protractor';
import { ZagatNewsletterSubscriptionWidget } from '../ui/ZagatNewsletterSubscriptionWidget';

export const SubscribeToNewsletter = {
    with: (emailAddress: string) =>
        Task.where(`#actor subscribes to the newsletter with ${ emailAddress }`,
            Enter.theValue(emailAddress).into(ZagatNewsletterSubscriptionWidget.initialEmailField),
            Click.on(ZagatNewsletterSubscriptionWidget.submitEmailButton),

        ),
};