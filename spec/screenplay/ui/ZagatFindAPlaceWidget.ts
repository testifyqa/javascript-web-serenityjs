import { Target } from "@serenity-js/protractor";
import { by } from "protractor";

export class ZagatFindAPlaceWidget {
    static initialSearchField = Target.the('initial search field').located(by.className('zgt-header-search-text'));
    static locationSearchField = Target.the('location search field').located(by.className('zgt-search-bar-location-term-input'));
}