import { Target } from "@serenity-js/protractor";
import { by } from "protractor";

export class ZagatNewsletterSubscriptionWidget {
    static initialEmailField = Target.the('initial email field').located(by.name('email'));
    static submitEmailButton = Target.the('submit email address button').located(by.className('zgt-newsletter-continue'));
    static zipCodeField = Target.the('zip code field').located(by.model('newslettersPageCtrl.zipcode'));
}