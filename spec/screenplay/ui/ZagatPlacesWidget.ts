import { Target } from "@serenity-js/protractor";
import { by } from "protractor";

export class ZagatPlacesWidget {
    static returnedResults = Target.the('search results container').located(by.className('zgt-place-results-list'));
    static noPlacesFound = Target.the('no places found message').located(by.className('disclaimer-headline'));
}