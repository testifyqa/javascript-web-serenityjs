import { Actor, Cast } from '@serenity-js/core';
import { BrowseTheWeb } from '@serenity-js/protractor';
import { protractor } from 'protractor';

export class Actors implements Cast { // actors come from/are a part of (implement) a Cast
    prepare(actor: Actor): Actor {
        return actor.whoCan( // actors have the ability to
            BrowseTheWeb.using(protractor.browser), // browse the web using the browser object from Protractor
        );
    }
}