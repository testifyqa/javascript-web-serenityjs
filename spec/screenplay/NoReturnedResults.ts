import { Text } from "@serenity-js/protractor";
import { ZagatPlacesWidget } from "./ui/ZagatPlacesWidget";

export const NoReturnedResults = () => Text.of(ZagatPlacesWidget.noPlacesFound);