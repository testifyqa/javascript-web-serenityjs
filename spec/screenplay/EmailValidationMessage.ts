import { Attribute } from "@serenity-js/protractor";
import { ZagatNewsletterSubscriptionWidget } from "./ui/ZagatNewsletterSubscriptionWidget";

export const EmailValidationMessage = () => Attribute.of(ZagatNewsletterSubscriptionWidget.initialEmailField).called('validationMessage');